# Primeiro Projeto: contador

Um app que tem um contador, e cada clique no botão
de adição, conta +1 e troca a cor da fonte e do 
background.

## Getting Started

Necessário que tenha a SDK do Flutter em seu computador, além de ter o Android Studio(Linux/Windows) e caso queira fazer um app para o IOS, necessário xCode(MacOs). Depois de ter instalado a SDK, basta rodar ele em sua IDE. Caso queira, editar, basicamente o código está na pasta "lib", basicamamente é isso. Todo o resto dos diretórios, são para um desenvolvimento mais avançado.

