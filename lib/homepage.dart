import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State {
  int counter = 0, red = 150, blue = 210, green = 10;

  @override
  Widget build(BuildContext context) {
    var white = Colors.white;

    return Container(
      color: Color.fromRGBO(green, red, blue * 2, 1.0),
      child: Center(
          child: GestureDetector(
        child: Text(
          'Counter: $counter',
          style: TextStyle(color: Color.fromRGBO(red, blue, green, 1.0)),
        ),
        onTap: () {
          setState(() {
            counter++;

            red = Random.secure().nextInt(255);

            if (red <= 50) {
              red += 20;
            }

            blue = Random.secure().nextInt(255);

            if (blue <= 50) {
              blue += 40;
            }

            green = Random.secure().nextInt(255);

            if (green <= 50) {
              green += 40;
            }
          });
        },
      )),
    );
  }
}
